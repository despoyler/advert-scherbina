<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Http\Requests\StoreAdvertRequest;
use App\Http\Requests\UpdateAdvertRequest;
use App\Metric;
use App\SubCategory;
use App\Transformers\AdvertTransformer;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AdvertController extends Controller
{
    public function index()
    {
        $adverts = Advert::all();


        return fractal()
            ->collection($adverts)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function show(Request $request, Advert $advert)
    {
        $refer = $request->hasHeader('HTTP_REFERER') ? $request->header('HTTP_REFERER') : '-';
        $metric = new Metric();
        $metric->setIpAddress($request->ip());
        $metric->setRefer($refer); //$request->get('userId', 'Unauthorized')
        $metric->setUserAgent($request->header('User-Agent'));
        $metric->setAdvert($advert);
        $metric->save();

        return $advert->getInfo();

    }

    public function store(StoreAdvertRequest $request)
    {
        $advert = new Advert();
        $subcategory = SubCategory::find($request->categoryId);
        if (!$subcategory)
            return response()->json([
                'status' => 'Category finding failed',
            ]);
        $advert->setTittle($request->tittle);
        $advert->setDescription($request->description);
        $advert->setImage($request->image);
        $advert->setPrice($request->price);
        $advert->setStatus(true);
        $advert->setUser($request->user());
        $advert->setCategory($subcategory);
        $advert->save();

        return $advert->getInfo();
    }

    public function update(UpdateAdvertRequest $request, Advert $advert)
    {
        if ($request->user()->cant('update', $advert)) {
            return response()->json([
                'status' => 'Unauthorized action'
            ]);
        }

        $advert->setTittle($request->get('tittle', $advert->getTittle()));
        $advert->setDescription($request->get('description', $advert->getDescription()));
        $advert->setImage($request->get('image', $advert->getImage()));
        $advert->setPrice($request->get('price', $advert->getPrice()));

        $advert->save();

        return $advert->getInfo();
    }

    public function destroy(Request $request, Advert $advert)
    {
        if ($request->user()->cant('delete', $advert)) {
            return response()->json([
                'status' => 'Unauthorized action'
            ]);
        }

        try {
            $advert->delete();
        } catch (\Exception $e) {
            Log::debug('Deleting failed');
        }
    }

    public function favorite(Request $request, Advert $advert)
    {

        $advert->favorites()->attach($request->userId);
    }
}
