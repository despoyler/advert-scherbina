<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Transformers\CommentTransformer;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CommentController extends Controller
{
    public function store(StoreCommentRequest $request, Advert $advert)
    {
        $comment = new Comment();
        $comment->setText($request->text);
        $comment->setUser($request->user());
        $comment->setAdvert($advert);
        $comment->save();

        return $comment->getInfo();
    }

    public function update(UpdateCommentRequest $request, Advert $advert, Comment $comment)
    {
        if ($request->user()->cant('update', $comment)) {
            return response()->json([
                'status' => 'Unauthorized action'
            ]);
        }

        $comment->setText($request->get('text', $comment->getText()));

        $comment->save();

        return $comment->getInfo();
    }

    public function destroy(Request $request, Advert $advert, Comment $comment)
    {
        if ($request->user()->cant('delete', $comment)) {
            return response()->json([
                'status' => 'Unauthorized action'
            ]);
        }

        try {
            $comment->delete();
        } catch (\Exception $e) {
            Log::debug('Deleting failed');
        }

    }
}
