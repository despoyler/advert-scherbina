<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Metric;
use App\Transformers\MetricTransformer;
use Illuminate\Http\Request;

class MetricController extends Controller
{
    public function index(Advert $advert){
        $metrics =$advert->getMetrics();

        return fractal()
            ->collection($metrics)
            ->transformWith(new MetricTransformer())
            ->toArray();
    }
}
