<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tittle' => 'string|max:200',
            'minPrice' => 'integer|max:200',
            'maxPrice' => 'integer|max:200',
            'mainCategoryId' => 'integer',
            'subCategoryId' => 'integer',
            'sortByPriceAsc' => 'string',
        ];
    }
}
