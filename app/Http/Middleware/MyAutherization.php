<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use \GuzzleHttp\Client;


class MyAutherization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if (is_null($token))
            return response()->json([
                'status' => 'No token',
            ]);

        $client = new Client(['base_uri' => 'auth.local/api/v1/']);

        $response = $client->request('GET', 'login', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorization' => $token,
            ]]);
        $jsonResponse = json_decode($response->getBody(), true);
        if ($jsonResponse['status'] === false)
            return response()->json([
                'status' => 'Auth failed',
            ]);
        $request->merge(['userId' => $jsonResponse['userId']]);

        $user = User::find($jsonResponse['userId']);
        $request->setUserResolver(function () use ($user) {
            return $user;
        });
        return $next($request);
    }
}
