<?php

namespace App;

use App\Interfaces\MainCategoryInterface;
use App\Transformers\SubCategoryTransformer;
use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model implements MainCategoryInterface
{
    public $timestamps = false;

    protected $fillable = [
        'text'
    ];

    public function subcategories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function getName(): string
    {
        return $this->text;
    }

    public function setName(string $name)
    {
        $this->text = $name;
    }

    public function getCategories()
    {
        $subcategories = $this->subcategories()->get();

        return fractal()
            ->collection($subcategories)
            ->transformWith(new SubCategoryTransformer())
            ->toArray();
    }
}
