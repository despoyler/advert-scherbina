<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 19:20
 */

namespace App\Interfaces;


use App\Advert;

interface MetricInterface
{
    public function getInfo(): array;

    public function getIpAddress(): string;

    public function getRefer();

    public function getUserAgent();

    public function setIpAddress(string $ipAddress);

    public function setRefer(string $refer);

    public function setUserAgent(string $userAgent);

    public function setAdvert(AdvertInterface $advert);

    public function getAdvert(): AdvertInterface;
}