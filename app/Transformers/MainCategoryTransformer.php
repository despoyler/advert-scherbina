<?php

namespace App\Transformers;

use App\MainCategory;
use League\Fractal\TransformerAbstract;

/**
 * Class MainCategoryTransformer
 * @package App\Transformers
 */
class MainCategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param MainCategory $mainCategory
     * @return array
     */
    public function transform(MainCategory $mainCategory)
    {
        return [
            'MainCategory' => $mainCategory->getName(),
        ];
    }
}
