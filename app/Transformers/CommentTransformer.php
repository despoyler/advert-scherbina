<?php

namespace App\Transformers;

use App\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    public function transform(Comment $comment){
        return [
            'text'=> $comment->getText(),
            'createdAt' => $comment->getCreateTime(),
            'updatedAt' =>$comment->getUpdateTime(),
            'byUser' => $comment->getAuthor()->getEmail()
        ];
    }
}
